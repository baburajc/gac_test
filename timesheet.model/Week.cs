﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace timesheet.model
{
    public class Week
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key]
        public int Id { get; set; }

        [Required]
        public int EffortId { get; set; }

        [StringLength(10)]
        [Required]
        public string WeekName { get; set; }

        [Required]
        public int Hours { get; set; }

        [ForeignKey("EffortId")]
        public Effort Effort { get; set; }
    }
}
